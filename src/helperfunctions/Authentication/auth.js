import axios from 'axios'

export function login(credentials){
	return new Promise((res, rej) => {
    axios.get('/sanctum/csrf-cookie')
		axios.post('/api/login',credentials).then( (response) => {
			//console.log(response.data)
			res(response.data);
		}).catch((error) => {
			rej(error)
		})
	})
}
