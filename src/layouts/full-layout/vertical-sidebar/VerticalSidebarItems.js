export default [ 
  {
    icon: "mdi-monitor-dashboard",
    title: "Dashboard",
    to: "/dashboard",
  },
  {
    icon: "mdi-account-multiple",
    title: "Customer",
    to: "/customer",
  },
  {
    icon: "mdi-notebook-edit-outline",
    title: "Task",
    to: "/task",
  },
  {
    icon: "mdi-email-plus-outline",
    title: "Send Email",
    to: "/emails",
  },
  {
    icon: "mdi-calendar",
    title: "Calendar",
    to: "/calendar",
  },
  {
    icon: "mdi-account-lock",
    title: "Roles and Permission",
    to: "/roles",
  },
];
