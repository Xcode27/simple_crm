import axios from 'axios';

export default {
    namespaced: true,
    state: {
        customers:[],
           
    },
    getters:{
        customers(state){
            return state.customers
        },
        
    },  
    mutations:{
        UPDATE_CUSTOMER_LIST(state,payload){
            state.customers = payload
        }
    },
    actions:{
       async getCustomerLists(context,param){

        const response = await axios.get('/api/customer',{
            headers:{
              'Authorization':`Bearer ${param.token}`,
              "Access-Control-Allow-Headers": "*",
               "Access-Control-Allow-Method" : "*",
               "Access-Control-Allow-Origin": "*",
               "Access-Control-Allow-Credentials": "*",
            }
          });

            if(response.status == 200){

                context.commit('UPDATE_CUSTOMER_LIST',response.data)
            }
       },
       async deleteCustomer(context,param){
        const response = await axios.get('/api/removeCustomer/'+param.id,{
            headers:{
              'Authorization':`Bearer ${param.token}`,
              "Access-Control-Allow-Headers": "*",
               "Access-Control-Allow-Method" : "*",
               "Access-Control-Allow-Origin": "*",
               "Access-Control-Allow-Credentials": "*",
            }
          });

            if(response.status == 200){
                return 'success'
            }else{
                return 'error'
            }
       },
       async addCustomer(context,param){
        const response = await axios.post('/api/customer',param.data,{
            headers:{
              'Authorization':`Bearer ${param.token}`,
              "Access-Control-Allow-Headers": "*",
               "Access-Control-Allow-Method" : "*",
               "Access-Control-Allow-Origin": "*",
               "Access-Control-Allow-Credentials": "*",
            }
          });

            if(response.status == 200){
                return 'success'
            }else{
                return 'error'
            }
       },
       async updateCustomer(context,param){
        const response = await axios.post('/api/customer/'+param.data.id,param.data,{
            headers:{
              'Authorization':`Bearer ${param.token}`,
              "Access-Control-Allow-Headers": "*",
               "Access-Control-Allow-Method" : "*",
               "Access-Control-Allow-Origin": "*",
               "Access-Control-Allow-Credentials": "*",
            }
          });

            if(response.status == 200){
                return 'success'
            }else{
                return 'error'
            }
       },
       async sendEmail(context,param){
        const response = await axios.post('/api/sendCustomerEmail',param.data,{
            headers:{
              'Authorization':`Bearer ${param.token}`,
              "Access-Control-Allow-Headers": "*",
               "Access-Control-Allow-Method" : "*",
               "Access-Control-Allow-Origin": "*",
               "Access-Control-Allow-Credentials": "*",
            }
          });

            if(response.status == 200){
                return 'success'
            }else{
                return 'error'
            }
       }
    }
}