import axios from 'axios';
import {getLocalUser} from '../../helperfunctions/Authentication/auth'

export default {
    namespaced: true,
    state: {
        authenticated:false,
        user:{}
           
    },
    getters:{
        authenticated(state){
            return state.authenticated
        },
        user(state){
            return state.user
        }
    },  
    mutations:{
        SET_AUTHENTICATED (state, value) {
            state.authenticated = value
        },
        SET_USER (state, value) {
            state.user = value
        }
    },
    actions:{
       
    }
}