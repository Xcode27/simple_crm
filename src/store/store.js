import Vue from "vue";
import Vuex from "vuex";
import menus from './Dashboards/menu'
import user from './User/user'
import customerStore from './Customer/customer'
import createPersistedState from 'vuex-persistedstate'

 Vue.use(Vuex);
export default new Vuex.Store({
    plugins:[
        createPersistedState()
    ],
    modules: {
        menus,
        user,
        customerStore
    },
})

 