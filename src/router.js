import Vue from "vue";
import Router from "vue-router";
import goTo from "vuetify/es5/services/goto";

Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  // This is for the scroll top when click on any router link
  scrollBehavior: (to, from, savedPosition) => {
    let scrollTo = 0;

    if (to.hash) {
      scrollTo = to.hash;
    } else if (savedPosition) {
      scrollTo = savedPosition.y;
    }

    return goTo(scrollTo);
  },
  // This is for the scroll top when click on any router link
  routes: [
    {
      path: "/",
      redirect: "Login",
      component: () => import("@/layouts/full-layout/Layout"),
      children: [
        // Application
        {
          name: "Dashboard",
          path: "dashboard",
          component: () => import("@/pages/user/dashboard"),
        },
        {
          name: "Customer",
          path: "/customer",
          component: () => import("@/pages/customer/customer"),
        },
        {
          name: "Emails",
          path: "/emails",
          component: () => import("@/pages/user/emailsender"),
        },
        {
          name: "EventCalendar",
          path: "/calendar",
          component: () => import("@/pages/user/calendar"),
        },
      ],
    },

    {
      path: "/",
      component: () => import("@/layouts/blank-layout/Blanklayout"),
      children: [
        {
          name: "Login",
          path: "Login",
          component: () => import("@/pages/authentication/login"),
        }
      ],
    },
    {
      path: "*",
      component: () => import("@/pages/authentication/Error"),
    },
  ],
});

import NProgress from "nprogress";

router.beforeResolve((to, from, next) => {
  // If this isn't an initial page load.
  if (to.name) {
    // Start the route progress bar.
    NProgress.start(800);
  }
  next();
});

router.afterEach(() => {
  // Complete the animation of the route progress bar.
  NProgress.done();
});

export default router;
