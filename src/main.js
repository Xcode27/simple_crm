import Vue from "vue";
import App from "./App.vue";
import vuetify from "./plugins/vuetify";
import router from "./router";
import store from "./store/store";
import Vuebar from "vuebar";
import "./plugins/base";
import VueSkycons from "vue-skycons";
import InstantSearch from "vue-instantsearch";
import axios from 'axios';
import VueAxios from 'vue-axios';
import VueToastr from "vue-toastr";
import CKEditor from '@ckeditor/ckeditor5-vue2';

Vue.use(VueSkycons, {
  color: "#1e88e5",
});
Vue.use(InstantSearch);
Vue.config.productionTip = false;
Vue.use(Vuebar);

Vue.use(VueToastr, {
  /* OverWrite Plugin Options if you need */
});

Vue.use(VueAxios, axios)
axios.defaults.baseURL = 'http://127.0.0.1:8000';
axios.withCredentials = true;
Vue.prototype.$eventHub = new Vue();

Vue.use( CKEditor);

new Vue({
  vuetify,
  store,
  router,
  render: (h) => h(App),
}).$mount("#app");
